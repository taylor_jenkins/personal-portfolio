

Taylor Jenkins
   taylorj91@live.com
   (801)-792-9048


Contents: 
	-README.txt
	-Resume.pdf
	-Cover_Letter.pdf
	-Restaurant_Employee_Management_Web_App
		-Contains gifs demonstrating a small portion of the projects i've worked on at
		 Standard Restaurant Supply (as noted on my resume), in which I have developed using
		 HTML, CSS, JavaScript, AngularJS, and Ruby on Rails with a PostgreSQL database.
	-Therastrive_Web_App
		-Contains gifs demonstrating the use of an administrator database interface using
		 Java, AngularJS, HTML, CSS, with an Amazon Firebase database.

